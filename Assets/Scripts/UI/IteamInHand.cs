using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// ����� ���������� UI ����������� ������
/// </summary>
public class IteamInHand : MonoBehaviour
{ 
    RectTransform RT;
    RawImage iteamImage;
    RawImage iconImage;

    /// <summary>
    /// ������� � ����(�� ������ ������)
    /// </summary>
    GameObject activeHandTool;

    private void Awake()
    {
        HeroController.ChangeHandToolEvent += ChangeToolInHand;
        ToolInHandBase.NewBulletEvent += ChangeIconImageBullet;
    }
    // Start is called before the first frame update
    void Start()
    {
        RT = GetComponent<RectTransform>();
        StartPosition();
        iteamImage = transform.GetChild(0).GetComponent<RawImage>();
        iconImage = transform.GetChild(1).GetComponent<RawImage>();
 
    }

    /// <summary>
    /// ������ ������ � ����, UI
    /// </summary>
    void ChangeToolInHand(GameObject oldHandTool, GameObject newHandTool)
    {
        SetImage(newHandTool.GetComponent<IteamOfTheWorld>().ImageIteam);
        activeHandTool = newHandTool;
    }

    void ChangeIconImageBullet(Texture2D texture)
    {
        if(texture == null)
        {
            texture = TempPrefab.singlton.NonTexture;
        }
        iconImage.texture = texture;
    }
    /// <summary>
    /// ������ ��������
    /// </summary>
    void SetImage(Texture2D image)
    {
        iteamImage.texture = image;
    }
    void StartPosition()
    {
        RT.position = new Vector3(Screen.width - 150, 80, 0);
    }
}
