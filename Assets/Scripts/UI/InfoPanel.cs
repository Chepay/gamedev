using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoPanel : MonoBehaviour
{
    GameObject UICircul; //��������� ���� �������
    GameObject UIPanel;
    public static InfoPanel singlton;

    private void Awake()
    {
        singlton = this;
    }

    private void Start()
    {
        
        UICircul = transform.GetChild(1).gameObject;
        UIPanel = transform.GetChild(0).gameObject;
        InfoControl(null);
    }

    /// <summary>
    /// ������ ��������� �� ������� � �������� � info
    /// </summary>
    /// <param name="obj"> �� </param>
    /// ���������� �������� ����������
    InfoObj CreateInfoInInfoPanel(GameObject obj)
    {
        InfoObj info = new InfoObj(); 
        ObjectsOfWorld objWorld = obj.GetComponent<ObjectsOfWorld>();
        info.Name = objWorld.Name;
        info.Health = new Vector2(objWorld.Health, objWorld.HealthMax);
        switch (objWorld.Type)
        {
            case ObjectsOfWorld.ObjectsOfType.Building:
                {
                    info.objType = ObjectsOfWorld.ObjectsOfType.Building;
                    if(obj.TryGetComponent(out GetterControl getter))
                    {
                        info.QuantityGetter = getter.EnergyBoxGetter.QuantityEnergy;
                    }
                    break;
                }
            case ObjectsOfWorld.ObjectsOfType.Resource:
                {
                    ResurceBase resurceBase = obj.GetComponent<ResurceBase>();
                    info.objType = ObjectsOfWorld.ObjectsOfType.Resource;
                    info.energyType = resurceBase.EnergyInfoSource.energyType;
                    info.QuantityGetter = resurceBase.EnergyInfoSource.QuantityEnergy;
                    break;
                }

        }
        return info;
    }
   /// <summary>
   /// ������� ������� ������ � ����������, �������������� �� ��� 
   /// </summary>
   /// <param name="obj">�� � ObjectsOfWorld</param>
    public void InfoControl(GameObject obj)
    {
        CleanInfoPanelUI();
        if (obj != null)
        {
            UIPanel.SetActive(true);
            UICircul.SetActive(true);
            InfoObj info = CreateInfoInInfoPanel(obj);
            ReadInfoInInfoPanel(info);
        }
    }

    /// <summary>
    /// ������� ������ ���������� �� ������
    /// </summary>
    void CleanInfoPanelUI()
    {
        Transform trans = this.transform.GetChild(0);
        for(int i = 0; i < trans.childCount; i++)
        {
            if (trans.GetChild(i).TryGetComponent(out Text text))
            {
                text.text = "";
            }
        }
        UICircul.SetActive(false);
        UIPanel.SetActive(false);
    }
   
    /// <summary>
    /// ����� ���������� �� �����
    /// </summary>
    /// <param name="info">����� ���������� � �������</param>
    void ReadInfoInInfoPanel(InfoObj info)
    {
        Transform trans = this.transform.GetChild(0);
        trans.GetChild(0).GetComponent<Text>().text = "���:        " + info.Name;
        trans.GetChild(1).GetComponent<Text>().text = "��������:   " + info.Health.ToString();
        trans.GetChild(2).GetComponent<Text>().text = "��� �������:" + info.objType.ToString();
        trans.GetChild(3).GetComponent<Text>().text = "���.�������:" + info.QuantityGetter.ToString();

    }

    /// <summary>
    /// ����� �������� ���������� �� ������� ��� ����������
    /// </summary>
    class InfoObj
    {
        public string Name;
        public Vector2 Health;
        public ObjectsOfWorld.ObjectsOfType objType;
        public EnergyType energyType;
        public int QuantityGetter;
    }
        
    }