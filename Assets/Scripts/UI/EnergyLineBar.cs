using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnergyLineBar : LineBar
{
    [HideInInspector]
    /// <summary>
    /// ������ �� �������������
    /// </summary>
    public EnergyBox energyBoxParent;

    private void Awake()
    {
        EnergyBox.ChangeOfEnergyEvent += ChangeOfEnergyBox;
    }
    private void Start()
    {
        MaxValue = Mathf.RoundToInt(transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.x);
        Color color = Color.white;
        switch (energyBoxParent.energyType)
        {
            case EnergyType.First:
                {
                    color = Color.blue;
                    break;
                }
            case EnergyType.Second:
                {
                    color = Color.green;
                    break;
                }
            case EnergyType.Third:
                {
                    color = Color.yellow;
                    break;
                }
        }
        transform.GetChild(0).GetComponent<RawImage>().color = color;
        ChangeOfEnergyBox(energyBoxParent);
    }

    private void ChangeOfEnergyBox(EnergyBox energyBox)
    {
        
        if (energyBoxParent == energyBox)
        {
            ChengesOfBar((float)100/energyBox.MaxQuantityEnergy * (energyBox.MaxQuantityEnergy - energyBox.QuantityEnergy));
        }
    }
}
