using UnityEngine;


public class LineBar : MonoBehaviour
{
    [HideInInspector]
    public float MaxValue;


    /// <summary>
    /// �������� �����
    /// </summary>
    /// <param name="x">������� �� ������� ������</param>
    public void ChengesOfBar(float x)
    {
        float p = MaxValue - (MaxValue / 100 * x);
        Vector2 delta = transform.GetChild(0).GetComponent<RectTransform>().sizeDelta;
        transform.GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(p, delta.y);
    }
}
