using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthLineBar : LineBar
{
    // Start is called before the first frame update
    void Start()
    {
        MaxValue = Mathf.RoundToInt(transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.x);
        HeroController.DamageOfHeroEvent += ChengesOfBar;
    }
}
