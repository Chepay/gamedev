using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// ����� ������, ������� ������ ��� ��������� ���� � �������
/// </summary>
public class HealthBar : MonoBehaviour
{
    /// <summary>
    /// ������ ����
    /// </summary>
    public GameObject PrefLineBar;
    public static HealthBar singlton;
    RectTransform RT;
    private Vector2 posBar;// = new Vector2(transform.localPosition.x + 220, Screen.height - 100);

    /// <summary>
    /// �������������� ��� �������
    /// </summary>
    
   // public GameObject UIHealthBar;
    private void Awake()
    {
        singlton = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        RT = this.gameObject.GetComponent<RectTransform>();
        StartPosition();
       // UIHealthBar = transform.GetChild(0).gameObject;
    }
    /// <summary>
    /// ��������� ������� ��������, �� ������ ���� ����� ����� �����
    /// </summary>
    void StartPosition()
    {
        RT.position = new Vector3(transform.localPosition.x+220, Screen.height - 40, 0);
        posBar = new Vector2(150, Screen.height - 100);
    }

    /// <summary>
    /// ������� �������������� ���
    /// </summary>
    public void CreateEnergyBar(EnergyBox energyBox)
    {
        GameObject g = Instantiate(PrefLineBar, this.transform);
        posBar = new Vector2(posBar.x, posBar.y - 60);
        g.transform.position = posBar;
        g.GetComponent<EnergyLineBar>().energyBoxParent = energyBox;
    } 
}
