using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshColliderControl : MonoBehaviour
{
    /// <summary>
    /// ����������� � ��������
    /// </summary>
    GameObject parentGO;
    /// <summary>
    /// �������������
    /// </summary>
    
    private void Start()
    {
        bool f = false;
        parentGO = this.gameObject;
        while(f != true)
        {
            f = FindParent(parentGO.transform.parent.gameObject);
        }
        
    }
    bool FindParent (GameObject j)
    {
        parentGO = j;
        if (j.TryGetComponent(out ObjectsOfWorld ob))
        {
            if(ob.AllParentObj == true)
            {
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// �������������
    /// </summary>
    /// <returns> ���������� ������������ ������ </returns>
    public GameObject Redirecting()
    {
        return parentGO;
    }
}
