using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// �������� ���� ������� �������� 
/// </summary>

public interface IWorldObject 
{
    /// <summary>
    /// ���������, ��������������
    /// </summary>
    public void Action(GameObject subject);
    /// <summary>
    /// �����, ������� ��������� �������� 
    /// </summary>
    public void StartObject();
    /// <summary>
    /// ��������� ��������� ��������
    /// </summary>
    public void StopObject();
    /// <summary>
    /// ������, ����������� ������� 
    /// </summary>
    public void DeadObject();
    /// <summary>
    /// �������� �������
    /// </summary>
    public void CreateObject();

}
