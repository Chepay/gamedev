using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// ��������� ������� ����������� � ������
/// </summary>
public interface IHandTool
{
    /// <summary>
    /// ������� ����� ������� ����
    /// </summary>
    public void ActionMouse0(HeroController subject);

    /// <summary>
    /// ������� ������ ������� ����
    /// </summary>
    public void ActionMouse1(HeroController subject);

    /// <summary>
    /// ������� �����������
    /// </summary>
    public void Recharge(HeroController subject);

    /// <summary>
    /// ������� ������ 
    /// </summary>
    public void Remove(HeroController subject);

    /// <summary>
    /// ������� �������
    /// </summary>
    public void Show(HeroController subject);

    /// <summary>
    /// ������� �������, ���
    /// </summary>
    public void BulletType(HeroController subject);
}
