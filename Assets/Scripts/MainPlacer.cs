using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainPlacer : MonoBehaviour
{
    public GameObject buildItem;
    public Camera eyes;
    public float distance;

    public Vector3 multiply(Vector3 a, Vector3 b) => new Vector3(a.x * b.x, a.y * b.y, a.z + b.z);

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            Ray ray = eyes.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
            RaycastHit hit = new RaycastHit();
            if (Physics.Raycast(ray, out hit, distance))
            {
                if (buildItem == null || !ChoosingBuilding.CanWeBuild(hit, buildItem))
                    return;
                GameObject temp = Instantiate(buildItem);
                temp.transform.position = hit.point;
                GetterControl getterControl = new GetterControl();
                if(temp.TryGetComponent(out getterControl))
                   // getterControl.energyType = hit.collider.gameObject.GetComponent<EnergyType>();
                temp.transform.Translate(multiply(hit.normal, temp.transform.localScale) / 2);
            }
        }
    }
}
