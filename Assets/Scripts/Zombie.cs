using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Zombie : ObjectsOfWorld
{
    public float damage;
    public float attackDist = 1;
    public string playerTag = "Player";
    private NavMeshAgent agent;
    private GameObject target;

    private void Start()
    {
        agent = gameObject.GetComponent<NavMeshAgent>();
    }

    private void Update()
    {
        UpdateTarget();
        WalkToTarget();
    }

    private void UpdateTarget()
    {
        target = GameObject.FindGameObjectWithTag(playerTag);
        GameObject[] players = GameObject.FindGameObjectsWithTag(playerTag);
        for (int i = 0; i < players.Length; i++)
            if (Vector3.Distance(players[i].transform.position, transform.position) < Vector3.Distance(target.transform.position, transform.position))
                target = players[i];
        transform.LookAt(target.transform);
    }

    private void WalkToTarget()
    {
        agent.SetDestination(target.transform.position);
        //start walking animation
        if(Vector3.Distance(target.transform.position, transform.position) <= attackDist)
        {
            target.GetComponent<ObjectsOfWorld>().HealthControll(-damage);
            //start attacking animation
        }
    }
}
