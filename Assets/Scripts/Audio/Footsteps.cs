using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Footsteps : MonoBehaviour
{

    [FMODUnity.EventRef]
    public string FootstepsSound = "";
    FMOD.Studio.EventInstance ftsptsInstance;
    private float runValue; // ���������� ��� ��������� ��� ��� ������  
    private RaycastHit rh; // �������� ����������� 
   // private float surfaceValue; //�������� ���������� ����������� 

    FMOD.Studio.EventDescription footstepsDescription;
    FMOD.Studio.PARAMETER_DESCRIPTION pdSURFACE;
    FMOD.Studio.PARAMETER_ID pIdSURFACE;


    

    
    void Start()
    {
        ftsptsInstance = FMODUnity.RuntimeManager.CreateInstance(FootstepsSound);

        footstepsDescription = FMODUnity.RuntimeManager.GetEventDescription("event:/pc/movement/footsteps");
        footstepsDescription.getParameterDescriptionByName("surface", out pdSURFACE);
        pIdSURFACE = pdSURFACE.id;
        
    }
    
    void PlayFootstepAndSetParameter()
    {
        if (Input.GetKeyDown(KeyCode.W))      
            ftsptsInstance.start(); //��������� ����

        if (Input.GetKeyUp(KeyCode.W))
            ftsptsInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT); //�������������

        if (Input.GetKeyDown(KeyCode.LeftShift))      
            runValue=1;
            ftsptsInstance.setParameterByName("running", runValue);
        if (Input.GetKeyUp(KeyCode.LeftShift))        
            runValue=0;
            ftsptsInstance.setParameterByName("running", runValue);


    }

    void MaterialCheck()
    {
        
                    
        
    }
    
    void Update()
    {
        Physics.Raycast(transform.position, Vector3.down, out rh, Mathf.Infinity); // ����� �������� lm (layer mask)
            Debug.DrawRay(transform.position, Vector3.down, Color.yellow);
        {
            if (rh.collider.tag == "grass")
            {
                ftsptsInstance.setParameterByID(pIdSURFACE, 0f);
            }

            if (rh.collider.tag == "gravel")
            {
                ftsptsInstance.setParameterByID(pIdSURFACE, 1f);
            }

            if (rh.collider.tag == "sand")
            {
                ftsptsInstance.setParameterByID(pIdSURFACE, 2f);

            }

            if (rh.collider.tag == "wood")
            {
                ftsptsInstance.setParameterByID(pIdSURFACE, 3f);
            }

            if (rh.collider.tag == "metal")
            {
                ftsptsInstance.setParameterByID(pIdSURFACE, 4f);
            }

            if (rh.collider.tag == "water")
            {
                ftsptsInstance.setParameterByID(pIdSURFACE, 5f);
              //  print("watersound");
            }

            if (rh.collider.tag == "snow")
            {
                ftsptsInstance.setParameterByID(pIdSURFACE, 6f);
            }

            if (rh.collider.tag == "ice")
            {
                ftsptsInstance.setParameterByID(pIdSURFACE, 7f);
            }

            if (rh.collider.tag == "liqud")
            {
                ftsptsInstance.setParameterByID(pIdSURFACE, 8f);
            }


            if (rh.collider.tag == "concrete")
            {
                ftsptsInstance.setParameterByID(pIdSURFACE, 9f);
            }

            else ftsptsInstance.setParameterByID(pIdSURFACE, 0f);

        }




            // MaterialCheck();
            PlayFootstepAndSetParameter();
        







        
    }
}
