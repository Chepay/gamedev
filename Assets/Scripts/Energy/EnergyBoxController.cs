using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
/// <summary>
/// ����� ���������� ��������� ������� ����� ��� � �������
/// </summary>
public class EnergyBoxController
{
    [Header("��� ������� ���������")]
    public List<EnergyBox> EnergyBoxList = new List<EnergyBox>();


    /// <summary>
    /// ���� ��������� ������������ � ��������
    /// </summary>
    /// <param name="box">�������, � ������� ���������� ���</param>
    /// <returns>��������� �������</returns>
    private EnergyBox FindFreeSpace(List<EnergyBox> energyBoxList, EnergyBox box)
    {
        for (int i = 0; i < energyBoxList.Count; i++)
        {
            EnergyBox energyBox = energyBoxList[i];
            if (energyBox.energyType == box.energyType)
            {
                if (energyBox.ChososingFreeSpace() == true)
                {
                    return energyBox;
                }
            }
        }
        return null;
    }
   
    /// <summary>
    /// ��������� �������� ������� �����
    /// </summary>
    /// <param name="acceptEnergy"> �������� </param>
    /// <param name="quantityGive"> ����������� </param>
    public void ActivatedExchangeEnergy(EnergyBox acceptEnergy, int quantityGive)
    {
        EnergyBox box = FindFreeSpace(EnergyBoxList, acceptEnergy);
        if (box != null)
        {
            box.ExchangeEnergy(acceptEnergy, quantityGive);
        }
    }

    /// <summary>
    /// ������� ���� ��� �������
    /// </summary>
    public void CreateEnergyLineBar()
    {
        for (int i = 0; i < EnergyBoxList.Count; i++)
        {
            HealthBar.singlton.CreateEnergyBar(EnergyBoxList[i]);
        }
    }
}
