using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ���� ���������� ����� ������(���������� � ������)
/// </summary>
public static class PriseEnergyController 
{
    /// <summary>
    /// �������� ������� ������� ��� �������, �������� ��� �� ���������� ������� ����� ���� ����� �� ������ �������
    /// </summary>
    /// <param name="energyBoxes"> ������ ���� ������� ���������</param>
    /// <param name="priseEnergyList"> ����� ���� �������� </param>
    /// <returns> ������ �������������� ������������� </returns>
    public static List<EnergyBox> ChoosingEnergy(List<EnergyBox> energyBoxes, List<PriseEnergy> priseEnergyList)
    {
        List<EnergyBox> TempEnergyBoxList = new List<EnergyBox>();
        for (int i = 0; i < priseEnergyList.Count; i++)
        {
            EnergyBox tempBox = FindEnergyBoxFromPrise(priseEnergyList[i], energyBoxes);
           if (tempBox != null)
            {
                TempEnergyBoxList.Add(tempBox);
            }
        }
        if(TempEnergyBoxList.Count == priseEnergyList.Count)
        {
            return TempEnergyBoxList;
        }
        return null;
    }

    /// <summary>
    /// ���� �����, ������� ����� ����������� � ������(�������)
    /// </summary>
    /// <param name="priseEnergy"> ���� </param>
    /// <param name="energyBoxes"> ������������(����) </param>
    /// <returns> ������ �������������� ������</returns>
    static EnergyBox FindEnergyBoxFromPrise(PriseEnergy priseEnergy, List<EnergyBox> energyBoxes)
    {
        for(int i = 0; i < energyBoxes.Count; i++)
        {
            EnergyBox energyBox = energyBoxes[i];
            if(energyBox.energyType == priseEnergy.Type && priseEnergy.Price <= energyBox.QuantityEnergy)
            {
                return energyBox;
            }
        }
        return null;
    }

    /// <summary>
    /// ������
    /// </summary>
    /// <param name="energyBoxes"> �������������� ����� </param>
    /// <param name="priseEnergyList"> ���� </param>
    public static void PurchaseFromEnergy(List<EnergyBox> energyBoxes, List<PriseEnergy> priseEnergyList)
    {
        for(int i = 0; i < energyBoxes.Count; i++)
        {
            energyBoxes[i].MinusEnergy(priseEnergyList[i].Price);
        }
    }

    /// <summary>
    /// ������� ��� ����
    /// </summary>
    public static void SumOfPrise(List<PriseEnergy> priseEnergyListBase, List<PriseEnergy> priseEnergyList)
    {
        for(int i = 0; i < priseEnergyListBase.Count; i++)
        {
            PriseEnergy priseBase = priseEnergyListBase[i];
            for(int i1 = 0; i1 < priseEnergyList.Count; i1++)
            {
                PriseEnergy priseList = priseEnergyList[i1];
                if(priseBase.Type == priseList.Type)
                {
                    ChengeOfPrise(priseBase, priseList.Price, true);
                }
            }
        }
    }

    /// <summary>
    /// ��������� ����
    /// </summary>
    /// <param name="prise"> ���� </param>
    /// <param name="quantity"> ���������� </param>
    static void ChengeOfPrise(PriseEnergy prise, int quantity) 
    {
        ChengeOfPrise(prise, quantity, false);
    }

    /// <summary>
    /// ��������� ����
    /// </summary>
    /// <param name="prise"> ���� </param>
    /// <param name="quantity"> ���������� </param>
    /// <param name="OnMinus"> ��������� ������������� �������� </param>

    static void ChengeOfPrise(PriseEnergy prise, int quantity, bool OnMinus)
    { 
        prise.Price += quantity;
        if (OnMinus == false)
        {
            if (prise.Price < 0)
            {
                prise.Price = 0;
            }
        }
        }
}
