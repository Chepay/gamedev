using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class EnergyBox
{
    
    public delegate void ChangeOfEnergy(EnergyBox energyBox);
    public static event ChangeOfEnergy ChangeOfEnergyEvent;

    [Header("��� �������")]
    public EnergyType energyType;

    [Header("����������� �������")]
    public int QuantityEnergy;
    [Header("������������ ����������� �������")]
    public int MaxQuantityEnergy;

    /// <summary>
    /// ��������� �������
    /// </summary>
    /// <param name="quantityPay"></param>
    /// <returns> ������� ����� </returns>
    public int MinusEnergy(int quantityPay)
    {
        if (quantityPay != 0)
        {
            int a = QuantityEnergy - quantityPay;
            QuantityEnergy -= quantityPay;
            ChoosingEnergyBox();
            if (a >= 0)
            {
                ChangeOfEnergyEvent(this);
                return quantityPay;
            }
            else
            {
                ChangeOfEnergyEvent(this);
                return quantityPay + a;
            }
        }
        ChangeOfEnergyEvent(this);
        return 0;
    }

    /// <summary>
    /// ����������������� � ���������
    /// </summary>
    /// <param name="quantityPay"> ����������� </param>
    public int AddEnergy(int quantityPay)
    {
        QuantityEnergy += quantityPay;
        ChoosingEnergyBox();
        ChangeOfEnergyEvent(this);
        return 0;
    }

    /// <summary>
    /// ������������ ��������, ���� ����� �� ���������
    /// </summary>
    private void ChoosingEnergyBox()
    {
        if (QuantityEnergy > MaxQuantityEnergy)
        {
            QuantityEnergy = MaxQuantityEnergy;
        }
        if (QuantityEnergy < 0)
        {
            QuantityEnergy = 0;
        }
    }

    /// <summary>
    /// ��������, ���� �� ��������� ����� � �������
    /// </summary>
    /// <returns></returns>
    public bool ChososingFreeSpace()
    {
        if (MaxQuantityEnergy - QuantityEnergy > 0)
        {
            return true;
        }
        return false;
    }

    /// <summary>
    /// �������� ������� �� ����� � ����
    /// </summary>
    /// <param name="acceptEnergy"> ������ �������� </param>
    /// <param name="quantityGive"> ������� </param>
    public void ExchangeEnergy(EnergyBox acceptEnergy, int quantityGive)
    {
        // ExchangeEnergy(this, acceptEnergy, quantityGive);
        int x = MaxQuantityEnergy - (QuantityEnergy + quantityGive);
        x = x < 0 ? x + quantityGive : quantityGive;
        AddEnergy(acceptEnergy.MinusEnergy(x));
    }

    /// <summary>
    /// �������� ������� �� ����� � ����
    /// </summary>
    /// <param name="giveEnergy"> ���� �������� </param>
    /// <param name="acceptEnergy"> ������ �������� </param>
    /// <param name="quantityGive"> ������� </param>
    public void ExchangeEnergy(EnergyBox giveEnergy, EnergyBox acceptEnergy, int quantityGive)
    {
        int x = giveEnergy.MaxQuantityEnergy - (giveEnergy.QuantityEnergy + quantityGive);
        x = x < 0 ? x + quantityGive : quantityGive;
      /*  if (x < 0)
        {
            x += quantityGive;
        }
        else x = quantityGive;*/
        giveEnergy.AddEnergy(acceptEnergy.MinusEnergy(x));
    }

    /// <summary>
    /// �������������� ��������, ����� �� ����� ���������� ��� ��������
    /// </summary>
    /// <param name="quantityPay"></param>
    /// <returns></returns>
    public int MathEnergy(int quantityPay)
    {
        if(quantityPay > 0)
        {
            return AddEnergy(quantityPay);
        }
        else
        {
            return MinusEnergy(quantityPay);
        }
    }

}

