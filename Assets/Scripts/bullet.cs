using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet : ObjectsOfWorld
{
    public BulletPropertys propertys;
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag != "Player")
        {
            GameObject GO = null;
            if(other.gameObject.tag == "Mesh")
            {
                GO = other.gameObject.GetComponent<MeshColliderControl>().Redirecting();
            }
            else
            {
                GO = other.gameObject;
            }
            GO.GetComponent<ObjectsOfWorld>().HealthControll(-propertys.damage);
        }
            Destroy(gameObject);
    }
}

