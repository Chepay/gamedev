using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// ����� ��� �������� ������ ��
/// </summary>
public class HeroController : ObjectsOfWorld
{
    /// <summary>
    /// ��� ���� ������� � �����
    /// </summary>
    public EnergyBoxController EnergyBoxesInHero = new EnergyBoxController();

    [HideInInspector]
    /// <summary>
    /// ��� ����������, ������� � ����� ������
    /// </summary>
    public InfoToHandTool infoHandTool;

    /// <summary>
    /// �������, �� ��������� �� ��������, ����� ��������� � InfoToHandTool
    /// </summary>
    public List<GameObject> HandTools = new List<GameObject>();

    /// <summary>
    /// �������, ��� ������� "����� ������"
    /// </summary>
    /// <param name="tool"> ����������, �� ������� ������ </param>
    /// <param name="oldTool"> ����������, ������� ������ </param>
    public delegate void NewHandTool(GameObject oldTool, GameObject tool);
    public static event NewHandTool ChangeHandToolEvent;

    /// <summary>
    /// ������� ����������� �����, � ���������� �����
    /// </summary>
    public delegate void DamageOfHero(float x);
    public static event DamageOfHero DamageOfHeroEvent;
    float OldHealth; // int to float

    private void Start()
    {
        ChangeOfHandTool(0);
        EnergyBoxesInHero.CreateEnergyLineBar();
    }

    /// <summary>
    /// ����������� ��������� �������� �����, � �������� ��� ����
    /// </summary>
    void HealthHero()
    {
        if (OldHealth != Health)
        {
            DamageOfHeroEvent(100 / HealthMax * (HealthMax - Health));
            OldHealth = Health;
        }
    }

    /// <summary>
    /// ��������� ������� �����������, ���� � �� ������� �������
    /// </summary>
    void ChangeOfHandTool(int x)
    {
        ChangeHandToolEvent(HandTools[infoHandTool.indexToolInventory], HandTools[changeIndexHand(x)]);
    }

    /// <summary>
    /// ��������� ������� ��������� ������
    /// </summary>
    /// <param name="a"> �� ������� ������ ������ </param>
    /// <returns> ����� ������, ����� �� �� ������, �� ��� �������� </returns>
    int changeIndexHand(int a)
    {
        infoHandTool.indexToolInventory += a;
        if (infoHandTool.indexToolInventory == HandTools.Count)
        {
            infoHandTool.indexToolInventory = 0;
        }
        if(infoHandTool.indexToolInventory < 0)
        {
            infoHandTool.indexToolInventory = HandTools.Count - 1;
        }
        return infoHandTool.indexToolInventory; 
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
            RaycastHit hit = new RaycastHit();
            if (Physics.Raycast(ray, out hit, 5))
            {
                if(hit.transform.TryGetComponent(out GetterBase getter))
                {
                    getter.Action(this.gameObject);
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.Q))
        {
            if (HandTools[infoHandTool.indexToolInventory].TryGetComponent<IHandTool>(out IHandTool tool))
            {
                tool.BulletType(this);
            }
        }
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            if(HandTools[infoHandTool.indexToolInventory].TryGetComponent<IHandTool>(out IHandTool tool))
            {
                tool.ActionMouse1(this);
            }
        }
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (HandTools[infoHandTool.indexToolInventory].TryGetComponent<IHandTool>(out IHandTool tool))
            {
                tool.ActionMouse0(this);
            }
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            ChangeOfHandTool(1);
        }

        //���������, ��� ������ ����������� �����
        if (Input.GetKeyDown(KeyCode.G))
        {
            HealthControll(-10);
            
        }
        if (Input.GetKeyDown(KeyCode.O))
        {
            HealthControll(10);
        }

        HealthHero();
    }
    [System.Serializable]
    public class InfoToHandTool
    {
        /// <summary>
        /// ������ ��������, ������� ������ � ����
        /// </summary>
        public int indexToolInventory;
    }
}
