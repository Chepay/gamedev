using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetterPlacer : MonoBehaviour
{
    public GameObject getter;
    public Camera eyes;
    public float distance;

    public Vector3 multiply(Vector3 a, Vector3 b) => new Vector3(a.x * b.x, a.y * b.y, a.z + b.z);

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            Ray ray = eyes.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
            RaycastHit hit = new RaycastHit();
            if (Physics.Raycast(ray, out hit, distance))
            {
                EnergyType energyType;
              //  GettingType gettingType;
                if (hit.collider.gameObject.TryGetComponent(out energyType))
                {
                    GameObject t = Instantiate(getter);
                    t.transform.position = hit.point;
                    t.GetComponent<Getter>().type = energyType;
                    t.transform.Translate(multiply(hit.normal, getter.transform.localScale) / 2);
                }
            }
        }
    }
}
