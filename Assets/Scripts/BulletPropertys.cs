using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BulletPropertys : System.ICloneable
{
    public float damage;
    public float speedOfBullet;

    public object Clone()
    {
        return new BulletPropertys
        {
            damage = this.damage,
            speedOfBullet = this.damage
        };
    }
    public void Add(BulletPropertys bullet)
    {
        damage += bullet.damage;
        speedOfBullet += bullet.speedOfBullet;
    }
}
