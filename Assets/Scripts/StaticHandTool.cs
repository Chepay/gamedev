using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class StaticHandTool
{
    /// <summary>
    /// �������� ���������
    /// </summary>
    /// <param name="distance"> ��������� ���� </param>
    /// ����������� ���� ����� ���� � ����
    public static void CreateTool(float distance, GameObject prefab, HeroController subject)
    {
       // Vector3 multiply(Vector3 a, Vector3 b) => new Vector3(a.x * b.x, a.y * b.y, a.z + b.z);
        Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
        RaycastHit hit = new RaycastHit();
        if (Physics.Raycast(ray, out hit, distance))
        {
            GameObject turret = prefab;
            if (turret != null)// � �������� �� null
            {
                List<EnergyBox> energyBoxList = PriseEnergyController.ChoosingEnergy(subject.EnergyBoxesInHero.EnergyBoxList, prefab.GetComponent<BuildBase>().Propertys.Prise);
                if (energyBoxList != null)
                {
                    if (ChoosingBuilding.PrefOfBuilding(hit, turret) == true)
                    {
                        PriseEnergyController.PurchaseFromEnergy(energyBoxList, prefab.GetComponent<BuildBase>().Propertys.Prise);
                        GameObject building = Object.Instantiate(turret);
                        building.name = turret.GetComponent<ObjectsOfWorld>().Name;
                        building.transform.position = hit.point;
                       // building.transform.Translate(multiply(hit.normal, turret.transform.localScale) / 2);//�� ����� �����?

                       building.transform.SetParent(ChoosingBuilding.ChoosingParent(hit.transform, building));//������� ��������, ���, ���� ������� ���
                        building.SetActive(true);
                        if (building.TryGetComponent(out GetterBase getterBase))
                        {
                            getterBase.ResurceObject = hit.transform.gameObject;//������ �� �� �������, ���� ����� �� ������
                        }
                    }
                }
            }
        }
    }

    /// <summary>
    /// �����
    /// </summary>
    /// <param name="distance"> ���������� </param>
    /// <param name="damsge"> ���� </param>
    ///    /// ����������� ���� ����� ���� � ����
    public static void Atacka(int damage, GameObject bullet, GameObject muzzle)
    {
        GameObject bulletG = Object.Instantiate(bullet);
        bulletG.transform.position = muzzle.transform.GetChild(0).position;
        bulletG.transform.rotation = muzzle.transform.rotation;
        Vector3 dir = bulletG.transform.forward;
        bulletG.GetComponent<Rigidbody>().AddForce(dir.normalized * 300);
        bulletG.GetComponent<bullet>().propertys.damage = -damage;
    }
}
