using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public CameraAndMouse cameraAndMouse;
    public MoveAndJump moveAndJump;

    [System.Serializable]
    public class CameraAndMouse
    {
        public CursorLockMode lockMouse = CursorLockMode.Locked;
        public float sensitivy;
        public Camera eyes;
        public float minRot = -90, maxRot = 90, rotAmount = 1.5f, stepAmount = 0.5f, amount = 5;
        [HideInInspector]
        public float rotationX = 0, rotationZ = 0, distantion;
        [HideInInspector]
        public Vector3 pos;
    }

    [System.Serializable]
    public class MoveAndJump
    {
        public float speed, gravity, jump;
        [HideInInspector]
        public CharacterController ch;
        [HideInInspector]
        public float deltaX, deltaY, deltaZ;
    }

    void Start()
    {
        Cursor.lockState = cameraAndMouse.lockMouse;
        cameraAndMouse.pos = transform.position;
        moveAndJump.ch = gameObject.GetComponent<CharacterController>();
    }

    private void Update()
    {
        Rotate();
        Move();
    }

    void Move()
    {
        Vector3 delta = new Vector3(moveAndJump.deltaX, 0, moveAndJump.deltaZ);
        if (Input.GetKeyDown(KeyCode.LeftShift))
            moveAndJump.speed *= 2;
        if (Input.GetKeyUp(KeyCode.LeftShift))
            moveAndJump.speed /= 2;
        moveAndJump.deltaY -= moveAndJump.gravity;
        if (moveAndJump.ch.isGrounded)
        {
            moveAndJump.deltaY = -0.1f;
            moveAndJump.deltaX = Input.GetAxisRaw("Horizontal") * moveAndJump.speed;
            moveAndJump.deltaZ = Input.GetAxisRaw("Vertical") * moveAndJump.speed;
            delta = new Vector3(moveAndJump.deltaX, 0, moveAndJump.deltaZ);
            delta = transform.TransformDirection(delta);
            delta = Vector3.ClampMagnitude(delta, moveAndJump.speed);
            moveAndJump.deltaX = delta.x;
            moveAndJump.deltaZ = delta.z;
            if (Input.GetKey(KeyCode.Space))
                moveAndJump.deltaY = moveAndJump.jump;
        }
        delta.y = moveAndJump.deltaY;
        delta *= Time.deltaTime;
        moveAndJump.ch.Move(delta);
    }

    void Rotate()
    {
        transform.Rotate(new Vector3(0, Input.GetAxis("Mouse X"), 0) * cameraAndMouse.sensitivy);
        cameraAndMouse.rotationX -= Input.GetAxis("Mouse Y") * cameraAndMouse.sensitivy;
        cameraAndMouse.rotationX = Mathf.Clamp(cameraAndMouse.rotationX, cameraAndMouse.minRot, cameraAndMouse.maxRot);
        cameraAndMouse.distantion += (transform.position - cameraAndMouse.pos).magnitude;
        cameraAndMouse.pos = transform.position;
        cameraAndMouse.rotationZ = Mathf.Sin(cameraAndMouse.distantion * moveAndJump.speed / cameraAndMouse.amount);
        cameraAndMouse.eyes.transform.localEulerAngles = new Vector3(cameraAndMouse.rotationX, 0, cameraAndMouse.rotationZ * cameraAndMouse.rotAmount);
        cameraAndMouse.eyes.transform.localPosition = new Vector3(cameraAndMouse.rotationZ * cameraAndMouse.stepAmount, cameraAndMouse.eyes.transform.localPosition.y, cameraAndMouse.eyes.transform.localPosition.z);
    }
}
