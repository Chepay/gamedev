using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretPlacer : MonoBehaviour
{
    public GameObject turret;
    public Camera eyes;
    public float distance;

    public Vector3 multiply(Vector3 a, Vector3 b) => new Vector3(a.x * b.x, a.y * b.y, a.z + b.z);

    private void Update()
    {
       /* if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            Ray ray = eyes.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
            RaycastHit hit = new RaycastHit();
            if (Physics.Raycast(ray, out hit, distance))
            {
                turret = ChoosingBuilding.PrefOfBuilding(hit);//������� ����������� �������
                if (turret != null)// � �������� �� null
                {
                    GameObject t = Instantiate(turret);
                    t.name = turret.GetComponent<ObjectsOfWorld>().Name;
                    t.transform.position = hit.point;
                    t.transform.Translate(multiply(hit.normal, turret.transform.localScale) / 2);
                    t.transform.SetParent(hit.transform);//������� ��������, ���, ���� ������� ���
                    if (t.TryGetComponent(out GetterBase getterBase))
                    {
                        getterBase.ResurceObject = hit.transform.gameObject;//������ �� �� �������, ���� ����� �� ������
                    }
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.Mouse0))//������� ����� ������ ����, ���� 2
        {
            Ray ray = eyes.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
            RaycastHit hit = new RaycastHit();
            if (Physics.Raycast(ray, out hit, distance))
            {
                if (hit.transform.TryGetComponent(out ObjectsOfWorld objectsOfWorld))
                {
                    objectsOfWorld.HealthControll(-2);
                }
            }
        }*/
        if (Input.GetKeyDown(KeyCode.Mouse3))
        {
            Ray ray = eyes.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
            RaycastHit hit = new RaycastHit();
            if (Physics.Raycast(ray, out hit, distance))
            {
                InfoPanel.singlton.InfoControl(hit.transform.gameObject);
            }
        }
        if (Input.GetKeyUp(KeyCode.Mouse3))
        {
            InfoPanel.singlton.InfoControl(null);
        }
    }
}

