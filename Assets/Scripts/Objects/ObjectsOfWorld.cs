using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectsOfWorld : MonoBehaviour
{
    public string Name;
    [Header("��� ���������")]
    public ObjectsOfType Type;
    [Header("��������� �������� ��������")]
    public bool CantChangeHealth;
    [Header("�������� �������")]
    public float Health;//int to float
    public float HealthMax;//int to float
    
    [Header("������ ����� �������")]
    public bool AllParentObj;
/// <summary>
/// �����������
/// </summary>
    [Header("����������� � ���������")]
    public Texture2D ImageIcon;

    /// <summary>
    /// ������ ��������, ���������� �� ������ ����� �������
    /// </summary>
    public List<IWorldObject> DeadList = new List<IWorldObject>();
 
    /// <summary>
    /// ���������� ���������, ����������� �����, ����, �������
    /// </summary>
    public void HealthControll(float damage)//int to float
    {
        if (!CantChangeHealth)
        {
            Health += damage;
            if (Health > HealthMax) Health = HealthMax;
            if (Health < 1) DeadControll();
        }
        print(damage + "   ::  " + Name);
    }

    /// <summary>
    /// ����������� �������� 
    /// </summary>
    public void SetPropertys(float health, float maxHealth, ObjectsOfType type, bool cantChangeHealth, Texture2D imageIcon) 
    {
        Health = health;
        HealthMax = maxHealth;
        Type = type;
        CantChangeHealth = cantChangeHealth;
        ImageIcon = imageIcon;
    }
    /// <summary>
    /// ���������� �������, ��� ���������� ��� ����������� �������, ���� �� ������ ������� �� ��� �� ���������, �� ����������� 
    /// ����������� ���������, ����� ����������� ��������� �� ������
    /// </summary>
    public void DeadControll()
    {
        if (DeadList.Count < 1)
        {
            //������������� ��������, ����� ������ ��������� � �����
            print($"{Name} {Type} �����!");
            if (Type != ObjectsOfType.Hero)
            {
                Destroy(this.gameObject);
            }
            
        }
        else
        {
            for(int i = 0; i < DeadList.Count; i++)
            {
                DeadList[i].DeadObject();
            }
        }
    }

    /// <summary>
    /// �������� �� ������� ������ 
    /// </summary>
    void ImageIconControl()
    {
        if(ImageIcon == null)
        {
            ImageIcon = TempPrefab.singlton.NonTexture;
        }
    }

    private void Start()
    {
        ImageIconControl();
    }
    /// <summary>
    /// ������ ���� ��������� ����� �������� � ����
    /// </summary>
    public enum ObjectsOfType
    {
        /// <summary>
        /// �����, ���
        /// </summary>
        Ground = 0,
        /// <summary>
        /// �����
        /// </summary>
        Wall = 1,
        /// <summary>
        /// ���������(������, ���������)
        /// </summary>
        Building = 2,
        /// <summary>
        /// ������
        /// </summary>
        Resource = 3, //������� resurce �� resource
       
        /// <summary>
        /// �����
        /// </summary>
        Hero = 4,

        /// <summary>
        /// ����
        /// </summary>
        Enemy = 5 // ������� �����
    }


    public ObjectsOfType AddInListObjectsOfType()
    {
        return ObjectsOfType.Building;
    }

}

