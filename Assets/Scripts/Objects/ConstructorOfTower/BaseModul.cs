using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

/// <summary>
/// ����� ����������� ���������� �������
/// </summary>
[System.Serializable]
[CreateAssetMenu(fileName = "Create", menuName = "Moduls/Base/ModulAll", order = 51)]
public class BaseModul : ParentOfModul, IController
{
    public void GeneralMetod(GameObject subject)
    {

    }
}

/// <summary>
/// �������� �������, ���������
/// </summary>
[System.Serializable]
public class PropertysOfModul : ICloneable
{
    /// <summary>
    /// ��� ������
    /// </summary>
    [Header("��� ������")]
    public TypeOfMobul TypeModul;
    /// <summary>
    /// ����� �������� �����
    /// </summary>
    [Header("������� �������� ���������")]
    public int Health;
    /// <summary>
    /// ��������, �� ������� ����� �������������
    /// </summary>
     [Header("�� ��� ����� �������")]
    public List<ObjectsOfWorld.ObjectsOfType> FloorType = new List<ObjectsOfWorld.ObjectsOfType>();
    /// <summary>
    ///  ��������, �� ������� ������ �������������
    /// </summary>
    [Header("�� ��� ������� ������")]
    public List<ObjectsOfWorld.ObjectsOfType> NoFloorType = new List<ObjectsOfWorld.ObjectsOfType>();
    /// <summary>
    /// �������� �������� ����(���� ����)
    /// </summary>
    [Header("�������� �������� ����(�������� ������������, ���� ������)")]
    public BoolVector3 SpeedRotateBody;
    /// <summary>
    /// �������� �������� �����
    /// </summary>
    [Header("�������� �������� �����(�������� �����������, ���� ������)")]
    public BoolVector3 SpeedRotateTower;
    /// <summary>
    /// �������� �������� ����
    /// </summary>
    [Header("�������� �������� ����")]
    public BoolVector3 SpeedRotateMuzle;
    /// <summary>
    /// ����� ������ �����
    /// </summary>
    public int Defence;
    /// <summary>
    /// ���������� ���� ������� ���������
    /// </summary>
    public Vector3 AnglesBuilding;
    /// <summary>
    /// ���������� � ����� �������� �� ����� 
    /// </summary>
    public BulletPropertys bulletPropertys;

    /// <summary>
    /// �������� �����������
    /// </summary>
    public float SpeedReload;
    /// <summary>
    /// ��� �������
    /// </summary>
    public TypeOfBullet typeOfBullet;
    /// <summary>
    /// ������ ����������� ����������
    /// </summary>
    public float CirklOfFind;
    /// <summary>
    /// ��������� ����
    /// </summary>
    public List<PriseEnergy> Prise = new List<PriseEnergy>();
    /// <summary>
    /// ������
   /// </summary>
    public GameObject Model;
    /// <summary>
    /// ������ �������
    /// </summary>
    public Texture2D Icon;

    /// <summary>
    /// ��������� �������� � ���������
    /// </summary>
    /// <param name="modul"> ��, ��� ��������� </param>
    public void Add(PropertysOfModul modul)
    {
        Health += modul.Health;
        FloorType = FloorType.Union(modul.FloorType).ToList();
        NoFloorType = NoFloorType.Union(modul.NoFloorType).ToList();
        SpeedRotateBody.Add(modul.SpeedRotateBody);
        SpeedRotateTower.Add(modul.SpeedRotateTower);
        SpeedRotateMuzle.Add(modul.SpeedRotateMuzle);
        typeOfBullet.Plus(modul.typeOfBullet);
        SpeedReload += modul.SpeedReload;
        Defence += modul.Defence;
        AnglesBuilding = Vector3.Max(AnglesBuilding, modul.AnglesBuilding);
        bulletPropertys.Add(modul.bulletPropertys);
        CirklOfFind += modul.CirklOfFind;
        PriseEnergyController.SumOfPrise(Prise, modul.Prise);
    }

    /// <summary>
    /// ����������� �������
    /// </summary>
    /// <returns></returns>
    public object Clone()
    {

        return new PropertysOfModul
        {
            Health = this.Health,
            FloorType = this.FloorType,
            SpeedRotateBody = this.SpeedRotateBody,
            SpeedRotateTower = this.SpeedRotateTower,
            SpeedRotateMuzle = this.SpeedRotateMuzle,
            Icon = this.Icon,
            Model = this.Model,
            NoFloorType = this.NoFloorType,
            TypeModul = this.TypeModul,
            Defence = this.Defence,
            SpeedReload = this.SpeedReload,
            AnglesBuilding = this.AnglesBuilding,
            bulletPropertys = this.bulletPropertys,
            CirklOfFind = this.CirklOfFind,
            Prise = this.Prise,
            typeOfBullet = this.typeOfBullet
            
        };
    }
    //
}
/// <summary>
/// ��� ������
/// </summary>
public enum TypeOfMobul
{
    Controller = 0,
    Modul = 1,
    Gun = 2,
    BaseOfBase = 3,
    BodyOfBase = 4,
    TowerOfBase = 5
}
[System.Serializable]
public class BoolVector3
{
    public Vector3 speedRotate;
    public Vector3 maxAngleRotate;
  //  public bool x;
  //  public bool y;
  //  public bool z;
    /// <summary>
    /// ��������� �������� true
    /// </summary>
   /* void AddTrue(BoolVector3 value) 
    {
        if (value.x == true) x = true;
        if (value.y == true) y = true;
        if (value.z == true) z = true;
    }*/
    public void Add(BoolVector3 value)
    {
        speedRotate += value.speedRotate;
        maxAngleRotate += value.maxAngleRotate;
        maxAngleRotate = CheckVector3(maxAngleRotate);
       // AddTrue(value);
    }
    Vector3 CheckVector3(Vector3 vector)
    {
        float a = vector.x;
        float b = vector.y;
        float c = vector.z;
        if (a > 360) a = 360;
        if (b > 360) b = 360;
        if (c > 360) c = 360;
        if (a < 0) a = 0;
        if (b < 0) b = 0;
        if (c < 0) c = 0;
        return new Vector3(a, b, c);
    }
}