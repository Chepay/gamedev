using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ������� ����� ���� �������
/// </summary>
[System.Serializable]
public abstract class ModulOfBase : ScriptableObject
{
    public abstract void MethodOfModul(GameObject subject);
}
