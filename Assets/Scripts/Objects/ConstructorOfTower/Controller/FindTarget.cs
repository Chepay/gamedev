using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ����� ������ ����� ������, ���� ���������
/// </summary>
public class FindTarget : FindTargetBase
{
    /// <summary>
    ///  ����� ����, ���� ��� ���� � ����, ������� ���������, ����� ������ ���������
    /// </summary>
    /// <param name="subject"></param>
    /// <param name="object"></param>
    /// <returns></returns>
    public override GameObject FindTargetUpdate(GameObject @subject, GameObject @object)
    {
        GameObject target = null; 
       /* if (TempPrefab.singlton.EnemyMass.transform.childCount > 0)
        {
            target = TempPrefab.singlton.EnemyMass.transform.GetChild(0).gameObject; //GameObject.FindGameObjectWithTag(mobType);
        }
        //GameObject[] mobs = GameObject.FindGameObjectsWithTag(mobType);
       // foreach (GameObject mob in TempPrefab.singlton.EnemyMass.transform)
       for(int i = 0; i < TempPrefab.singlton.EnemyMass.transform.childCount; i++)
       {
            GameObject mob = TempPrefab.singlton.EnemyMass.transform.GetChild(i).gameObject;
            float distanseMob = Vector3.Distance(@subject.transform.position, mob.transform.position);
            float distanseTarget = Vector3.Distance(@subject.transform.position, target.transform.position);
            MonoBehaviour.print(distanseMob + "   :   " + @subject.GetComponent<Turret>().Propertys.CirklOfFind);
            if (distanseMob < distanseTarget && distanseMob <= @subject.GetComponent<Turret>().Propertys.CirklOfFind)
                target = mob;
       }*/
        if (TempPrefab.singlton.EnemyMass.transform.childCount > 0)
        {
            float circklOfFind = @subject.GetComponent<Turret>().Propertys.CirklOfFind;
            for (int i = 0; i < TempPrefab.singlton.EnemyMass.transform.childCount; i++)
            {
                GameObject g = TempPrefab.singlton.EnemyMass.transform.GetChild(i).gameObject;
                float distanseG = Vector3.Distance(@subject.transform.position, g.transform.position);
                if (target == null)
                {
                    if(circklOfFind > distanseG)
                    {
                        target = g;
                    }
                }
                else
                {
                    float distanseTarget = Vector3.Distance(@subject.transform.position, target.transform.position);
                    if (distanseG < distanseTarget)
                        target = g;
                }
            }

        }
            return target; //target;GameObject.FindGameObjectWithTag("Player");
    }
}
