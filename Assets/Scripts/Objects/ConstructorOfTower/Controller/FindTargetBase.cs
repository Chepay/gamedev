using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Create", menuName = "Moduls/Controller/Target1", order = 52)]
public abstract class FindTargetBase : ScriptableObject
{
    public abstract GameObject FindTargetUpdate(GameObject @subject, GameObject @object);
    
 
}
