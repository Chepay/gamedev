using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/// <summary>
/// ����� ���������� �������������, ���� ���������� �� �������
/// </summary>
public class ConstructorOfBase : MonoBehaviour
{
    public PropertysOfModul Propertys = new PropertysOfModul();
    public ParentOfModul Controller;
   // public BaseModul Controller;
    public List<ParentOfModul> TowerLevelOne = new List<ParentOfModul>();
    public List<ParentOfModul> TowerLevelTwo = new List<ParentOfModul>();
    public List<ParentOfModul> TowerLevelTree = new List<ParentOfModul>();

    [Header("���������, ������ �����")]
    public GameObject GunHero;


    /// <summary>
    /// ����� ����������� � ��������
    /// </summary>
    /// <returns></returns>
    ParentOfModul FindControllerInMassModul(TypeOfMobul type)
    {
        ParentOfModul modul = TowerLevelOne.Where(u => u.Propertys.TypeModul == type).FirstOrDefault();
        if(modul == null)
        {
            modul = TowerLevelTwo.Where(u => u.Propertys.TypeModul == type).FirstOrDefault();
        }
        if(modul == null)
        {
            modul = TowerLevelTree.Where(u => u.Propertys.TypeModul == type).FirstOrDefault();
        }
        return modul;
    }

    private void Start()
    {
        Controller = FindControllerInMassModul(TypeOfMobul.Controller);
        SortThroughMass();
        CreateTurret();
    }

    /// <summary>
    /// ���� � ������ ���������� �� �������
    /// </summary>
    /// <param name="moduls"> ������ </param>
    void FindAndReadModuls(List<ParentOfModul> moduls)
    {
        for (int i = 0; i < moduls.Count; i++)
        {
            ParentOfModul modul = moduls[i];
            Propertys.Add(modul.Propertys);
        }
    }

    /// <summary>
    /// ������� ���� ��������, �� ������� �� ��������
    /// </summary>
    void SortThroughMass()
    {
        FindAndReadModuls(TowerLevelOne);
        FindAndReadModuls(TowerLevelTwo);
        FindAndReadModuls(TowerLevelTree);
    }
    /// <summary>
    /// �������� ������
    /// </summary>
    void CreateTurret()
    {
        GameObject BaseTurret = new GameObject("Turret");
        BaseTurret.name = "Turrdet";
        BaseTurret.transform.position = new Vector3(0, 0, 0);
        BaseTurret.transform.SetParent(this.transform);
    
        Turret turret = BaseTurret.AddComponent<Turret>();
        turret.ControllerModul = Controller;
        turret.Propertys = (PropertysOfModul)Propertys.Clone();
        turret.Propertys.Model = BaseTurret;
        turret.AllParentObj = true;
        
        turret.Name = "Turel";
        
        ParentOfModul modul = TowerLevelOne.Where(u => u.Propertys.TypeModul == TypeOfMobul.BaseOfBase).FirstOrDefault();
        GameObject baseT = Instantiate(modul.Propertys.Model, transform.position, transform.rotation, BaseTurret.transform) as GameObject;
        ParentOfModul modulBody = TowerLevelTwo.Where(u => u.Propertys.TypeModul == TypeOfMobul.BodyOfBase).FirstOrDefault();
        GameObject bodyT = Instantiate(modulBody.Propertys.Model, baseT.transform.GetChild(1).GetChild(0).position, transform.rotation, baseT.transform) as GameObject;
        ParentOfModul modulTower = TowerLevelTree.Where(u => u.Propertys.TypeModul == TypeOfMobul.TowerOfBase).FirstOrDefault();
        GameObject towerT = Instantiate(modulTower.Propertys.Model, bodyT.transform.GetChild(1).GetChild(0).position, modulTower.Propertys.Model.transform.rotation, bodyT.transform) as GameObject;
        turret.SetPropertys();
        turret.AddPropertis(FabricaOfBullet.CreateBullet(turret.Propertys.typeOfBullet));
        GunHero.GetComponent<TCreaterTurret>().BulletAll.Add(BaseTurret);
        GameObject defTarget = new GameObject("DefTarget");
        defTarget.transform.SetParent(BaseTurret.transform);
        defTarget.transform.position = new Vector3(2, 2, 0);
        turret.Parts.SetDefTarget(defTarget);
        BaseTurret.SetActive(false);

        //������� ������� ������, ��������� ����� ����, ���������� ��� ���������� � ������


    }
}
