using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

/// <summary>
/// ����� ����������� ���������� �������
/// </summary>
[System.Serializable]
[CreateAssetMenu(fileName = "Create", menuName = "Moduls/Base/Controller1", order = 51)]
public class ControllerModul : ParentOfModul
{

}

/// <summary>
/// �������� �������, ���������
/// </summary>
[System.Serializable]
public class PropertysOfModul : ICloneable
{
    /// <summary>
    /// ��� ������
    /// </summary>
    [Header("��� ������")]
    public TypeOfMobul TypeModul;
    /// <summary>
    /// ����� �������� �����
    /// </summary>
    [Header("������� �������� ���������")]
    public int Health;
    /// <summary>
    /// ��������, �� ������� ����� �������������
    /// </summary>
     [Header("�� ��� ����� �������")]
    public List<ObjectsOfWorld.ObjectsOfType> FloorType = new List<ObjectsOfWorld.ObjectsOfType>();
    /// <summary>
    ///  ��������, �� ������� ������ �������������
    /// </summary>
    [Header("�� ��� ������� ������")]
    public List<ObjectsOfWorld.ObjectsOfType> NoFloorType = new List<ObjectsOfWorld.ObjectsOfType>();
    /// <summary>
    /// �������� �������� ����(���� ����)
    /// </summary>
    [Header("�������� �������� ����(�������� ������������)")]
    public Vector3 SpeedRotateBody;
    /// <summary>
    /// �������� �������� �����
    /// </summary>
    [Header("�������� �������� �����(�������� �����������)")]
    public Vector3 SpeedRotateTower;
    /// <summary>
    /// ���� �������� ����
    /// </summary>
    public Vector3 AnglesRotateBody;
    /// <summary>
    /// ���� �������� �����
    /// </summary>
    public Vector3 AnglesRotateTower;
    /// <summary>
    /// ����� ������ �����
    /// </summary>
    public int Defence;
    /// <summary>
    /// ���������� ���� ������� ���������
    /// </summary>
    public Vector3 AnglesBuilding;
    /// <summary>
    /// �������� ��������, �������� �� ����� 
    /// </summary>
    public int SpeedOfShot;
    /// <summary>
    /// ������ ����������� ����������
    /// </summary>
    public float CirklOfFind;
    /// <summary>
    /// ��������� ����
    /// </summary>
    public List<PriseEnergy> Prise = new List<PriseEnergy>();
    /// <summary>
    /// ������
   /// </summary>
    public GameObject Model;
    /// <summary>
    /// ������ �������
    /// </summary>
    public Texture2D Icon;

    /// <summary>
    /// ��������� �������� � ���������
    /// </summary>
    /// <param name="modul"> ��, ��� ��������� </param>
    public void Add(PropertysOfModul modul)
    {
        Health += modul.Health;
        FloorType = FloorType.Union(modul.FloorType).ToList();
        NoFloorType = NoFloorType.Union(modul.NoFloorType).ToList();
        SpeedRotateBody += modul.SpeedRotateBody;
        SpeedRotateTower += modul.SpeedRotateTower;
        AnglesRotateBody = Vector3.Max(AnglesRotateBody, modul.AnglesRotateBody);
        AnglesRotateTower = Vector3.Max(AnglesRotateTower, modul.AnglesRotateTower);
        Defence += modul.Defence;
        AnglesBuilding = Vector3.Max(AnglesBuilding, modul.AnglesBuilding); 
        SpeedOfShot += modul.SpeedOfShot;
        CirklOfFind += modul.CirklOfFind;
        PriseEnergyController.SumOfPrise(Prise, modul.Prise);

    }

    /// <summary>
    /// ����������� �������
    /// </summary>
    /// <returns></returns>
    public object Clone()
    {

        return new PropertysOfModul
        {
            Health = this.Health,
            FloorType = this.FloorType,
            SpeedRotateBody = this.SpeedRotateBody,
            AnglesRotateBody = this.AnglesRotateBody,
            SpeedRotateTower = this.SpeedRotateTower,
            AnglesRotateTower = this.AnglesRotateTower,
            Icon = this.Icon,
            Model = this.Model,
            NoFloorType = this.NoFloorType,
            TypeModul = this.TypeModul,
            Defence = this.Defence,
            AnglesBuilding = this.AnglesBuilding,
            SpeedOfShot = this.SpeedOfShot,
            CirklOfFind = this.CirklOfFind,
            Prise = this.Prise
        };
    }
    //
}
/// <summary>
/// ��� ������
/// </summary>
public enum TypeOfMobul
{
    Controller = 0,
    Modul = 1,
    Gun = 2,
    BaseOfBase = 3,
    BodyOfBase = 4,
    TowerOfBase = 5
}