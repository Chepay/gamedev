using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ����� ��� ��������� ������ ������, �������� �������
/// </summary>
public class TurretPrefab : MonoBehaviour
{
    GameObject g; 
    [Header("����� ������")]
    public PartsOfTurret Parts;
    private void Start()
    {
        g = this.gameObject;
        bool b = false;
        while(b == false)
        {
            b = FindParent(g);
        }
        if(g.TryGetComponent(out BuildBase turret))
        {
            turret.Parts.SetParts(Parts);
        }
    }
    /// <summary>
    /// ����������� ����� ������� 
    /// </summary>

    bool FindParent(GameObject j)
    {
    
        if (j.TryGetComponent(out ObjectsOfWorld ob))
        {
            if (ob.AllParentObj == true)
            {
                return true;
            }
        }
        else
        {
            g = g.transform.parent.gameObject;
        }
        return false;
    }
}
