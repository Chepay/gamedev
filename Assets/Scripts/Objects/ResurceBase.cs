using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ����� ������� �������
/// </summary>
public class ResurceBase : ObjectsOfWorld
{
    [Header("���������� �� ������� � ���������")]
    public EnergyBox EnergyInfoSource;
    [Header("�������� ������������� �������� � ���������(����� ����� �����)")]
    public int SpeedCreateSource;
    //  [Header("����������� ������� � ���������")]
    //public int QuantityResurce;
    //[Header("��� ��������")]
    //public EnergyType energyType;

    float timeSource = 0;

 

    /// <summary>
    /// ������� ������� ������� � ���������
    /// </summary>
    private void CreateUnitEnergy()
    {
        if(timeSource > SpeedCreateSource)
        {
            EnergyInfoSource.AddEnergy(1);
            timeSource = 0;
        }
        else
        {
            timeSource += Time.deltaTime;
        }
    }

    private void Update()
    {
        CreateUnitEnergy();
    }

}
