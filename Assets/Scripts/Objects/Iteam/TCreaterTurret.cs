using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TCreaterTurret : ToolInHandBase, IHandTool
{
    public void ActionMouse0(HeroController subject)
    {
        if (BulletPref == null)
        {
            ChoosingBullet(0);
        }
        StaticHandTool.CreateTool(5, BulletPref, subject);
    }  

    public void ActionMouse1(HeroController subject)
    {
        
    }

    public void BulletType(HeroController subject)
    {
        ChoosingBullet(1);
    }

    public void Recharge(HeroController subject)
    {
        throw new System.NotImplementedException();
    }

    public void Remove(HeroController subject)
    {
        throw new System.NotImplementedException();
    }

    public void Show(HeroController subject)
    {
        throw new System.NotImplementedException();
    }

}
