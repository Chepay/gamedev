using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ����� ����� ��� ���� ���������, ������� �������� � ���������, ������� ����� ������ � ����
/// </summary>
public class IteamOfTheWorld : MonoBehaviour
{

    /// <summary>
    /// ���
    /// </summary>
    public string Name;
    /// <summary>
    /// �����������
    /// </summary>
    public Texture2D ImageIteam;

    /// <summary>
    /// ��� ��������
    /// </summary>
    public TypeOfIteam Type;
    
    /// <summary>
    /// �������� �� ����������(� ����)
    /// </summary>
    [HideInInspector]
    public bool ActiveIteam;

    private void Start()
    {
        HeroController.ChangeHandToolEvent += ChangeActivated;
    }

    /// <summary>
    /// ��������� � ����������� ��������
    /// </summary>
    /// <param name="newTool"></param>
    /// <param name="oldTool"></param>
    void ChangeActivated(GameObject oldTool, GameObject newTool)
    {
        if (oldTool == this.gameObject)
        {
            ActiveIteam = false;
        }
        if (newTool == this.gameObject)
        {
            ActiveIteam = true;
        }
      
    }

    /// <summary>
    /// ���� ���������
    /// </summary>
    public enum TypeOfIteam
    {
        tool = 0,
        gun = 1,
        grenade = 2,
    }
}
