using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// ������� ����� ���������, ��� ����� ����� � ����
/// </summary>
public class ToolInHandBase : IteamOfTheWorld
{
    public delegate void NewBullet(Texture2D texture);
    public static event NewBullet NewBulletEvent;
    /// <summary>
    /// ������ ���� ��������� �������
    /// </summary>
    [Header("������ ���� ��������� �������")]
    public List<GameObject> BulletAll = new List<GameObject>();

    /// <summary>
    /// ��������� �����
    /// </summary>
    [HideInInspector]
    public GameObject BulletPref;
    
    /// <summary>
    /// ������ ��������� ������ � �������
    /// </summary>
    int indexBullet;

    private void Awake()
    {
        HeroController.ChangeHandToolEvent += ChangeBullet;
    }

    /// <summary>
    /// ��������� ���� ��� ����� ������
    /// </summary>
    /// <param name="oldTool"> ������ ������ </param>
    /// <param name="newTool"> ����� ������ </param>
    void ChangeBullet(GameObject oldTool, GameObject newTool)
    {
        if(newTool == this.gameObject)
        {
            ChoosingBullet(0);
        }
    }

    /// <summary>
    /// ��������� ������� �������
    /// </summary>
    /// <param name="newIndex"> �� ������� ������ ������ </param>
    /// <returns>����� ������</returns>
    int ChengeindexBullet(int newIndex)
    {
        indexBullet += newIndex;
        if(indexBullet >= BulletAll.Count)
        {
            indexBullet = 0;
        }
        if(indexBullet < 0)
        {
            indexBullet = BulletAll.Count - 1;
        }
        return indexBullet;
    }

    /// <summary>
    /// ����� ��������
    /// </summary>
    /// <returns></returns>
    public GameObject ChoosingBullet(int newIndex)
    {
        BulletPref = BulletAll[ChengeindexBullet(newIndex)];
        if (BulletPref.TryGetComponent<ObjectsOfWorld>(out ObjectsOfWorld objectsOfWorld))
        {
            NewBulletEvent(objectsOfWorld.ImageIcon);
        }
        return BulletPref;
    }

    /// <summary>
    /// ���������� �������
    /// </summary>
    /// <param name="indexBullet">����� �������</param>
    /// <returns>������ �������</returns>
    public GameObject ChekingBullet(int indexBullet)
    {
        return BulletPref = BulletAll[indexBullet];
    }
}


