using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ������ ����������
/// </summary>
public class GetterBase : BuildBase, IWorldObject
{
    public EnergyBox EnergyBoxGetter;
    // public string Name;
    /// <summary>
    /// ����� ������ ������ ��������
    /// </summary>
    [Header("�������� ������")]
    public float TimeOfGetting;
    /// <summary>
    /// ������� �������� ��������
    /// </summary>
    [Header("��������� ���������")]
    public int PowerOfGetter;
    /// <summary>
    /// ������ �� �� ��������� �������
    /// </summary>
    [HideInInspector]
    public GameObject ResurceObject;

    private  float timer;

    public virtual void Action(GameObject subject)
    {
        print($"{Name} �����");
        if(subject.TryGetComponent(out HeroController hero))
        {
            hero.EnergyBoxesInHero.ActivatedExchangeEnergy(EnergyBoxGetter, EnergyBoxGetter.QuantityEnergy);

        }
        
    }

    public void CreateObject()
    {
        throw new System.NotImplementedException();
    }

    public void DeadObject()
    {
        print("����   " + this.gameObject.name);
    }

    public void StartObject()
    {
        throw new System.NotImplementedException();
    }

    public void StopObject()
    {
        throw new System.NotImplementedException();
    }


    /// <summary>
    /// ���������� ��������
    /// </summary>
    void TimerControl()
    {
        if (timer > -1)
        {
            timer += Time.deltaTime;
            if (timer >= TimeOfGetting)
            {
                EnergyBoxGetter.ExchangeEnergy(transform.parent.GetComponent<ResurceBase>().EnergyInfoSource, PowerOfGetter);
                timer = 0;
            }
        }
    }
    private void Update()
    {
        TimerControl();
    }
    void Start()
    {
        DeadList.Add(this);
        EnergyBoxGetter.energyType = ResurceObject.GetComponent<ResurceBase>().EnergyInfoSource.energyType;
    }
}
