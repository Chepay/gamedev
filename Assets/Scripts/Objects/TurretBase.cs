using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    /// <summary>
    /// ������� ����� ��� �������
    /// </summary>
public abstract class TurretBase : BuildBase
{
    public void SetPropertys()
    {
        base.SetPropertys(Propertys.Health, Propertys.Health, ObjectsOfType.Building, false, null);
    }
}

/// <summary>
/// ��������� ����� ������
/// </summary>
[System.Serializable]
public class PartsOfTurret
{
    /// <summary>
    /// ����
    /// </summary>
    public GameObject Muzzle;
    /// <summary>
    /// �����
    /// </summary>
    public GameObject Tower;
    /// <summary>
    /// ����
    /// </summary>
    public GameObject Body;
    /// <summary>
    /// ���������
    /// </summary>
    public GameObject BaseT;
    /// <summary>
    /// ���� �� ���������
    /// </summary>
    public GameObject DefTarget;
    
    /// <summary>
    /// ��������� �������� 
    /// </summary>
    /// <param name="parts"> ������</param>
    public void SetParts(PartsOfTurret parts)
    {
        if(parts.Muzzle != null) Muzzle = parts.Muzzle;
        if(parts.Tower != null) Tower = parts.Tower;
        if(parts.Body != null) Body = parts.Body;
        if(parts.BaseT != null) BaseT = parts.BaseT;
        if(parts.DefTarget != null) DefTarget = parts.DefTarget;
    }

    public void SetDefTarget(GameObject defTarget)
    {
        if(DefTarget == null && defTarget != null) DefTarget = defTarget;
    }
}