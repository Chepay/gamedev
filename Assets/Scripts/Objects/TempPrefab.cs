using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// ��������� ������ �� ����� ���������, ����� ��� �������� ��������
/// </summary>
public class TempPrefab : MonoBehaviour
{
    public static TempPrefab singlton;
    [Header("������ ������")]
    public GameObject Turret;
    [Header("������ ���������")]
    public GameObject Getter;

    /// <summary>
    /// ������� ��������
    /// </summary>
    public GameObject TurretMass;
    public GameObject EnemyMass;
    /// <summary>
    /// �������� �����������
    /// </summary>
    [Header("�������� �����������")]

    public Texture2D NonTexture;

    private void Awake()
    {
        singlton = this;
    }
}

