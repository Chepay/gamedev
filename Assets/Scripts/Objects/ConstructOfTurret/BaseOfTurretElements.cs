using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// �����, ���� ��� ���� �������� ��������� ����� ��� ���������������, � ����� �� ���������
/// </summary>
public class BaseOfTurretElements : MonoBehaviour
{
    public BaseOfTurretElementInfo InfoElement = new BaseOfTurretElementInfo();
   


}

[System.Serializable]
public class BaseOfTurretElementInfo
{
    public string NameElements;
    public TypeOfElementsTurret TypeElement;
    public Vector2 Health;

    [Header("������ ���� ���������")]
    public List<PriseEnergy> Prise = new List<PriseEnergy>();

    public Mesh Model;

    /// <summary>
    /// ����� ���������� ���������� � ������� ����� ���������
    /// </summary>
    /// <param name="health"> �������� </param>
    /// <param name="prise"> ���� </param>
    public void Add(int health, List<PriseEnergy> prise)
    {
        Health = new Vector2(Health.x, Health.y + health);
        //����
        PriseEnergyController.SumOfPrise(Prise, prise);
    }
}

/// <summary>
///���� ��������� ����� 
/// </summary>
public enum TypeOfElementsTurret
{
    /// <summary>
    /// ���������
    /// </summary>
    Base = 0,
    /// <summary>
    /// ����, ��������
    /// </summary>
    Body = 1,
    /// <summary>
    /// ��������� �����
    /// </summary>
    Tower = 2,
}

