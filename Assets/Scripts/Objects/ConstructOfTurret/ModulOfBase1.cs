using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class  ModulOfBase1 : ScriptableObject
{

    public InfoModulBase InfoModul = new InfoModulBase();

   

    
    public abstract void Cr();

}
[System.Serializable]
public class InfoModulAll
{
    /// <summary>
    /// ��� ������
    /// </summary>
    public string NameModul;
    /// <summary>
    /// ��� ������, ��� ����� ����� �� ������������
    /// </summary>
    public TypeOfElementsTurret TypeElement;
    /// <summary>
    /// ������� �������� ���������
    /// </summary>
    public int AddHealth;

    public List<PriseEnergy> Prise = new List<PriseEnergy>();

    public Mesh Model;

}
[System.Serializable]
public class InfoModulBase : InfoModulAll
{
    /// <summary>
    /// ���� ������������, �� ������� ����� ������ ������� ������
    /// </summary>
    public List<ObjectsOfWorld.ObjectsOfType> ObjectOfFloor = new List<ObjectsOfWorld.ObjectsOfType>();
    /// <summary>
    /// �����������, �� ������� ������ �������
    /// </summary>
    public List<ObjectsOfWorld.ObjectsOfType> NonObjectOfFloor = new List<ObjectsOfWorld.ObjectsOfType>();
}