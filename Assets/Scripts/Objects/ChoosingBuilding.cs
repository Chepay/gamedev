using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// ����� ��� ������ ��� ����� �������
/// </summary>
public static class ChoosingBuilding
{
    /// <summary>
    /// ����� ��� ������ ������� � ��������, ����� �� ������� ���� ��� ��������
    /// </summary>
    /// <param name="hit"> ��������� ����� </param>
    /// <returns> ����, ������� ������ </returns>
    public static GameObject PrefOfBuilding(RaycastHit hit)
    {
        Transform hitTransform = hit.transform;
        GameObject pref = null;
        if (hitTransform.TryGetComponent(out ObjectsOfWorld objectsOfWorld))
        {
            pref = WhatWeAreBuilding(objectsOfWorld.Type);
        }
        return pref;
    }

    /// <summary>
    /// ��������, ����� �� ���� ������� ������
    /// </summary>
    /// <param name="hit"> �� ��� ������ </param>
    /// <param name="build">��� ������</param>
    /// <returns>����� ��� ���</returns>
    public static bool PrefOfBuilding(RaycastHit hit, GameObject build)
    {
        if (build.TryGetComponent(out BuildBase building))
        {
            if (hit.transform.TryGetComponent(out ObjectsOfWorld floor))
            {
                if(building.Propertys.FloorType.IndexOf(floor.Type) > -1) return true;
            }
            else return false;
        }
        else return false;
        return false;
    }

    //�������� �����
    public static bool CanWeBuild(RaycastHit hit, GameObject prefab)
    {
        if (hit.transform.TryGetComponent(out ObjectsOfWorld objectsOfWorld))
        {
            switch (objectsOfWorld.Type)
            {
                case ObjectsOfWorld.ObjectsOfType.Ground:
                    {
                        if (prefab.name.ToLower() == TempPrefab.singlton.Turret.name.ToLower())
                            return true;
                        return false;
                    }
                case ObjectsOfWorld.ObjectsOfType.Resource:
                    {
                        if (prefab.name.ToLower() == TempPrefab.singlton.Turret.name.ToLower() || prefab.name.ToLower() == TempPrefab.singlton.Getter.name.ToLower())
                            return true;
                        return false;
                    }
                case ObjectsOfWorld.ObjectsOfType.Building:
                    {
                        // ����� ����� ������ ����� ���� �������, �� ����� ����� ������


                        return false;
                    }
                case ObjectsOfWorld.ObjectsOfType.Wall:
                    {
                        // ����� ����� ������ ����� ���� �������, �� ����� ����� ������                          
                        return false;
                    }
            }
            return false;
        }
        return false;
    }

    /// <summary>
    /// �������� ����� ������, ��� ������
    /// </summary>
    /// <param name="type"> ��� ������� �� ������� ����� ������� </param>
    /// <returns>������ ���������������� �������</returns>
    public static GameObject WhatWeAreBuilding(ObjectsOfWorld.ObjectsOfType type)
    {
        switch (type)
        {
            case ObjectsOfWorld.ObjectsOfType.Ground:
                {
                    return TempPrefab.singlton.Turret;
                }

            case ObjectsOfWorld.ObjectsOfType.Resource:
                {
                    return TempPrefab.singlton.Getter;
                }
        }
        return null;
    }

    /// <summary>
    /// �������� ��������
    /// </summary>
    public static Transform ChoosingParent(Transform hit, GameObject build)
    {
        if (build.TryGetComponent<Turret>(out Turret turret))
        {
            return TempPrefab.singlton.TurretMass.transform;//����� ����� �������, �� ���� ���
        }
        if(build.TryGetComponent<GetterBase>(out GetterBase getter))
        {
            return hit;
        }
        return hit;
    }
}
