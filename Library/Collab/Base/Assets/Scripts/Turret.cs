using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : BuildBase
{
    public Transform muzzle; //����
    public GameObject bullet;
    public string mobType;
    public GameObject target;
    public float timeToShoot;
    private float deltatime = 0;


    private void Update()
    {
        deltatime -= Time.deltaTime;
        if (deltatime < 0)
        {
            UpdateTarget();
            ShootTarget();
            deltatime = timeToShoot;
        }
    }

    public void UpdateTarget()
    {
        target = GameObject.FindGameObjectWithTag(mobType);
        GameObject[] mobs = GameObject.FindGameObjectsWithTag(mobType);
        foreach (GameObject mob in mobs)
            if (Vector3.Distance(transform.position, mob.transform.position) < Vector3.Distance(transform.position, target.transform.position))
                target = mob;
        if (target != null)
        {
            // transform.LookAt(target.transform);
            Vector3 rot = transform.localEulerAngles;
            transform.LookAt(target.transform);
            transform.localEulerAngles = new Vector3(rot.x, transform.localEulerAngles.y, rot.z);
            muzzle.LookAt(target.transform.Find("Head"));
            //muzzle.GetChild(0).localEulerAngles += new Vector3(90, 0, 0);
        }
    }

    public void ShootTarget()
    {
        if (target != null)
        {
            GameObject b = Instantiate(bullet);
            b.transform.position = muzzle.GetChild(0).position;
            b.transform.rotation = muzzle.rotation;
            Vector3 dir = b.transform.forward/* + new Vector3(0, Vector3.Distance(transform.position, target.transform.position) * b.GetComponent<Rigidbody>().mass / 9.8f, 0)*/;
            b.GetComponent<Rigidbody>().AddForce(dir.normalized * 500);
        }
    }
}
